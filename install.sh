#!/bin/bash

echo "Usage: ./install.sh base_data"

export DEV_BASE=$1

ssl_folder=$DEV_BASE/nginx/ssl

mkdir -p $DEV_BASE/nginx/conf.d
mkdir -p $ssl_folder/certs
mkdir -p $ssl_folder/private
mkdir -p $DEV_BASE/pgadmin/pgadmin
mkdir $DEV_BASE/postgres

#cp ./nginx-selfsigned.crt $ssl_folder/certs/
#cp ./nginx-selfsigned.key $ssl_folder/private/
cp ./app.conf $DEV_BASE/nginx/conf.d/
